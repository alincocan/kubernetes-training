package com.demo.accountinformation.resource;

import com.demo.accountinformation.remote.Account;
import com.demo.accountinformation.remote.AccountClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("accounts")
public class AccountResource {

    private final AccountClient accountClient;

    public AccountResource(AccountClient accountClient) {
        this.accountClient = accountClient;
    }

    @GetMapping
    public Flux<Account> getAccounts() {
        return accountClient.getAccounts();
    }
}
