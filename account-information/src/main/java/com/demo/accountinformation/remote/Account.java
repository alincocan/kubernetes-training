package com.demo.accountinformation.remote;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private String id;
    private String iban;
    private String name;
    private String currency;
    private List<Balance> balances;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Balance {
        private double amount;
        private String currency;
    }
}
