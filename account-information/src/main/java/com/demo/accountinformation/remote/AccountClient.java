package com.demo.accountinformation.remote;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Component
public class AccountClient {

    private final WebClient webClient;

    public AccountClient() {
        this.webClient = WebClient.create("http://account-datasource-service.k8slab.svc.cluster.local:8080");
    }

    public Flux<Account> getAccounts() {
        return webClient.get()
            .uri("/accounts")
            .retrieve()
            .bodyToFlux(Account.class);
    }
}
