package com.demo.accountdatasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountDatasourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountDatasourceApplication.class, args);
	}

}
