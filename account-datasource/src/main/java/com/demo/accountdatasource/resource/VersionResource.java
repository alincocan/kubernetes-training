package com.demo.accountdatasource.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionResource{

    @GetMapping("version")
    public String getVersion() {
        return "v1.2";
    }
}
