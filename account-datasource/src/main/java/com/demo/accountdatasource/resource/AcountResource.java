package com.demo.accountdatasource.resource;

import com.demo.accountdatasource.repository.AccountDataSourceRepository;
import com.demo.accountdatasource.repository.model.Account;
import com.demo.accountdatasource.resource.dto.AccountResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;

@RestController
@RequestMapping("accounts")
public class AcountResource {

    private final AccountDataSourceRepository accountDataSourceRepository;

    public AcountResource(AccountDataSourceRepository accountDataSourceRepository) {
        this.accountDataSourceRepository = accountDataSourceRepository;
    }

    @GetMapping()
    public Flux<AccountResponse> getAccounts() {
        return accountDataSourceRepository.findAll()
            .map(this::mapAccountToAccountResponse);
    }

    private AccountResponse mapAccountToAccountResponse(Account account) {
        var balances = account
            .getBalances()
            .stream()
            .map(this::mapBalanceToBalanceResponse)
            .collect(Collectors.toList());

        return AccountResponse
            .builder()
            .id(account.getId())
            .iban(account.getIban())
            .name(account.getName())
            .currency(account.getCurrency())
            .balances(balances)
            .build();
    }

    private AccountResponse.BalanceResponse mapBalanceToBalanceResponse(Account.Balance balance) {
        return AccountResponse.BalanceResponse
            .builder()
            .currency(balance.getCurrency())
            .amount(balance.getAmount())
            .build();
    }
}
