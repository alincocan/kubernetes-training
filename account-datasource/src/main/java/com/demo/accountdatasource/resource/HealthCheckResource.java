package com.demo.accountdatasource.resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
@Slf4j
public class HealthCheckResource {

    private AtomicInteger cont = new AtomicInteger(0);
    private static final int MAX_SUCCESS = 4;
    @GetMapping("healthCheck")
    public ResponseEntity<Void> healthCheck() {
        cont.getAndIncrement();
        if(cont.get() <= MAX_SUCCESS ) {
            log.info("healthcheck - 200 OK");
            return ResponseEntity.ok().build();
        } else {
            log.error("healthcheck - 500 InternalServerError");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
