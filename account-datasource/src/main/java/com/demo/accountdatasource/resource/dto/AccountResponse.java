package com.demo.accountdatasource.resource.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class AccountResponse {
    private String id;
    private String iban;
    private String name;
    private String currency;
    private List<BalanceResponse> balances;

    @Getter
    @Setter
    @Builder
    public static class BalanceResponse {
        private double amount;
        private String currency;
    }
}
