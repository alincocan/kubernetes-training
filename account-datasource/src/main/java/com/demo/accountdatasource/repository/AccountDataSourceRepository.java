package com.demo.accountdatasource.repository;

import com.demo.accountdatasource.repository.model.Account;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Repository
public class AccountDataSourceRepository {

    private List<Account> accounts;

    public AccountDataSourceRepository() {
        accounts = new ArrayList<>();
        accounts.add(buildAccount("DE75512108001245126199", 2345.02));
        accounts.add(buildAccount("RO09BCYP0000001234567890", 989.78));
        accounts.add(buildAccount("RO09BCYP1111001230639290", 57849.10));
        accounts.add(buildAccount("RO09BCYP0101001453247811", 900.0));
    }

    public Flux<Account> findAll() {
        return Flux.fromIterable(accounts);
    }

    private Account buildAccount(String iban, double amount) {
       return Account
           .builder()
           .id(UUID.randomUUID().toString())
           .currency("EUR")
           .iban(iban)
           .name("John Doe")
           .balances(Collections.singletonList(
               Account.Balance
                   .builder()
                   .amount(amount)
                   .currency("EUR")
                   .build()
           ))
           .build();
    }
}
