package com.demo.accountdatasource.repository.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class Account {
    private String id;
    private String iban;
    private String name;
    private String currency;
    private List<Balance> balances;

    @Getter
    @Setter
    @Builder
    public static class Balance {
        private double amount;
        private String currency;
    }
}
